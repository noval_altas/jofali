<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeModel extends CI_Model {

	public function tampil()
	{
		return $this->db->get('menu')->result();
	}

	public function find($id)
	{
		$result = $this->db->where('idmenu', $id)
					->limit(1)
					->get('menu');
		if ($result->num_rows() > 0) {
			return $result->row();
		} else {
			return array();
		}
	}

}