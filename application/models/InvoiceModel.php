<?php defined('BASEPATH') OR exit('No direct script access allowed');

class InvoiceModel extends CI_Model
{
    public function index()
    {
        date_default_timezone_set('Asia/Jakarta');
        $nama = $this->input->post('nama');
        $notlp = $this->input->post('notlp');
        $notlp = $this->input->post('notlp');
        $pesan = $this->input->post('pesan');
        $hartot = $this->cart->total();

        $invoice = array(
            'nama' => $nama,
            'notlp' => $notlp,
            'pesan' => $pesan,
            'aksi' => 'ambil',
            'hargatotal' => $hartot,
            'tglpesan' => date('Y-m-d H:i:s')
        );
        $this->db->insert('tb_invoice', $invoice);
        $id_invoice = $this->db->insert_id();

        foreach ($this->cart->contents() as $item) {
            $data = array(
                'id_invoice' => $id_invoice,
                'id_brg' => $item['id'],
                'nama_brg' => $item['name'],
                'jumlah' => $item['qty'],
                'harga' => $item['price'],
            );
            $this->db->insert('tb_pesanan', $data);
        }
        return TRUE;
    }

    public function dikirim()
    {
        $data1['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        date_default_timezone_set('Asia/Jakarta');
        $nama = $this->input->post('nama');
        $notlp = $this->input->post('notlp');
        $alamat = $this->input->post('alamat');
        $pesan = $this->input->post('pesan');
        $hartot = $this->cart->total()+5000;

        $invoice = array(
            'nama' => $nama,
            'notlp' => $notlp,
            'alamat' => $alamat,
            'pesan' => $pesan,
            'aksi' => 'dikirim',
            'hargatotal' => $hartot,
            'tglpesan' => date('Y-m-d H:i:s')
        );
        $this->db->insert('tb_invoice', $invoice);
        $id_invoice = $this->db->insert_id();

        $data = array(
            'alamat' => $alamat,
        );

        $this->db->where('id', $data1['user']['id'])->update('user', $data);

        foreach ($this->cart->contents() as $item) {
            $data = array(
                'id_invoice' => $id_invoice,
                'id_brg' => $item['id'],
                'nama_brg' => $item['name'],
                'jumlah' => $item['qty'],
                'harga' => $item['price'],
            );
            $this->db->insert('tb_pesanan', $data);
        }
        return TRUE;
    }

    public function tampil_data()
    {
        $result = $this->db->get('tb_invoice');
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return false;
        }
    }

    public function ambil_id_invoice($idinvoice)
    {
        $result = $this->db->where('idinvoice', $idinvoice)->get('tb_invoice');
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return false;
        }
    }

    public function ambil_id_pesanan($idinvoice)
    {
        $result = $this->db->where('id_invoice', $idinvoice)->get('tb_pesanan');
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return false;
        }
    }
}