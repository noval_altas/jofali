<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AdminModel extends CI_Model
{
    public function countproduk()
    {
        $this->db->SELECT('COUNT(namamenu) as total');
        $this->db->FROM('menu');
        return $this->db->get()->row()->total;
    }
    
    public function countuser()
    {
        $this->db->SELECT('COUNT(nama) as total');
        $this->db->FROM('user');
        return $this->db->get()->row()->total;
    }
    
    public function counttotal()
    {
        $this->db->SELECT('COUNT(idinvoice) as total');
        $this->db->FROM('tb_invoice');
        return $this->db->get()->row()->total;
    }

    public function countharga()
    {
        $this->db->SELECT('SUM(hargatotal) AS total');
        $this->db->FROM('tb_invoice');
        return $this->db->get()->row()->total;
    }
}