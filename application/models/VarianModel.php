<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VarianModel extends CI_Model {

	public function varian()
	{
		return $this->db->get('jenismenu')->result();
	}

	public function insertjenis($data,$table)
	{
		return $this->db->insert($table, $data);
	}

	public function edit($where,$table)
	{
		return $this->db->get_where($table,$where);
	}

	public function delete($where,$table)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}

	public function update($where,$data,$table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}
}