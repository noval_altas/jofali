<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MenuModel extends CI_Model {

	public function menu()
	{
		$query = "SELECT `menu`.*, `jenismenu`.`namajenis`
				FROM `menu` JOIN `jenismenu`
				ON `menu`.`idjenismenu` = `jenismenu`.`idjenismenu`";

		return $this->db->query($query)->result();
	}

	public function jenismenu()
	{
		return $this->db->get('jenismenu')->result();
	}

	public function insertmenu($data)
	{
		return $this->db->insert('menu',$data);
	}

	public function editmenu($idmenu)
	{
		return $this->db->get_where('menu', array('idmenu' => $idmenu))->row();
	}

	public function delete($where,$table)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}


}