<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* ====================================================================== */

// function login_link()
// {
//     $ci = get_instance()
//     $user = $ci->db->->get_where('peserta', ['nim_peserta' => $this->session->userdata('nim_peserta')])->row_array();

//     if (!$ci->session->userdata('nim_peserta')) {
//         if ($user['hakakses'] == 0) {
//             redirect('login');
//             if ($user['status'] == 0) {
//                 redirect('vote');
//             } else {
//                 redirect('login');                
//             }
//         } else {
//             redirect('adminmpn');
//         }
//     }
// }

if (! function_exists('jenis_kelamin')) {

    function jenis_kelamin($jk)
    {
        if ($jk == 'l') {

            return 'Laki - Laki';
        }

        return 'Perempuan';
    }

}

if (! function_exists('status_produk')) {

    function status_produk($sv)
    {
        if ($sv == 'ambil') {

            return '<span class="badge badge-success">Diambil</span>';
        }

        return '<span class="badge badge-warning">Dikirim</span>';
    }

}

if (! function_exists('hakakses')) {

    function hakakses($ha)
    {
        if ($ha == '0') {

            return '<span class="badge badge-info">User</span>';
        }

        return '<span class="badge badge-danger">Admin</span> <span class="badge badge-info">User</span>';
    }

}