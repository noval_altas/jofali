  <!-- ////////////////////////////////////////////////////////////////////////////-->
  <footer class="footer footer-static footer-light navbar-border">
    <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
      <span class="float-md-right d-block d-md-inline-block">Copyright &copy; <?= date('Y')?> <a class="text-bold-800 grey darken-2" href="http://jofali.xyz/"
        target="_blank">JOFALICOFFEE
    </p>
  </footer>
  <!-- BEGIN VENDOR JS-->
  <script src="<?= base_url('admin_template/');?>app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <script src="<?= base_url('admin_template/');?>app-assets/vendors/js/charts/raphael-min.js" type="text/javascript"></script>
  <script src="<?= base_url('admin_template/');?>app-assets/vendors/js/charts/morris.min.js" type="text/javascript"></script>
  <script src="<?= base_url('admin_template/');?>app-assets/vendors/js/extensions/unslider-min.js" type="text/javascript"></script>
  <script src="<?= base_url('admin_template/');?>app-assets/vendors/js/timeline/horizontal-timeline.js" type="text/javascript"></script>
  <script src="<?= base_url('admin_template/');?>app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
  <script src="<?= base_url('admin_template/');?>app-assets/vendors/js/extensions/dropzone.min.js" type="text/javascript"></script>
  <script src="<?= base_url('admin_template/');?>app-assets/vendors/js/ui/prism.min.js" type="text/javascript"></script>
  <script src="<?= base_url('admin_template/');?>app-assets/vendors/js/extensions/sweetalert.min.js" type="text/javascript"></script>
  <script src="<?= base_url('admin_template/');?>app-assets/vendors/js/extensions/jquery.steps.min.js" type="text/javascript"></script>
  <script src="<?= base_url('admin_template/');?>app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js" type="text/javascript"></script>
  <script src="<?= base_url('admin_template/');?>app-assets/vendors/js/pickers/daterange/daterangepicker.js" type="text/javascript"></script>
  <script src="<?= base_url('admin_template/');?>app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
  <script src="<?= base_url('admin_template/');?>app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
  <script src="<?= base_url('admin_template/');?>app-assets/vendors/js/forms/validation/jquery.validate.min.js" type="text/javascript"></script>
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN STACK JS-->
  <script src="<?= base_url('admin_template/');?>app-assets/js/core/app-menu.js" type="text/javascript"></script>
  <script src="<?= base_url('admin_template/');?>app-assets/js/core/app.js" type="text/javascript"></script>
  <script src="<?= base_url('admin_template/');?>app-assets/js/scripts/customizer.js" type="text/javascript"></script>
  <!-- END STACK JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <script src="<?= base_url('admin_template/');?>app-assets/js/scripts/pages/dashboard-ecommerce.js" type="text/javascript"></script>
  <script src="<?= base_url('admin_template/');?>app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>
  <script src="<?= base_url('admin_template/');?>app-assets/js/scripts/extensions/dropzone.js" type="text/javascript"></script>
  <script src="<?= base_url('admin_template/');?>app-assets/js/scripts/extensions/sweet-alerts.js" type="text/javascript"></script>
  <script src="<?= base_url('admin_template/');?>app-assets/js/scripts/forms/wizard-steps.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL JS-->

  <script type="text/javascript">
    $(".removejenis").on('click',function(){
      swal({
          title: "Apakah Anda Yakin?",
          text: "Anda tidak akan dapat memulihkan file ini!",
          icon: "warning",
          showCancelButton: true,
          buttons: {
                  cancel: {
                      text: "Tidak, Batal!",
                      value: null,
                      visible: true,
                      className: "btn-warning",
                      closeModal: false,
                  },
                  confirm: {
                      text: "Ya, Hapus!",
                      value: true,
                      visible: true,
                      className: "",
                      closeModal: false
                  }
          }
      }).then(isConfirm => {
          if (isConfirm) {
              window.location.href = $(this).data('href');
              swal("Hapus!", "File Anda telah dihapus.", "success");
          } else {
              swal("Batal", "File Anda aman:)", "error");
          }
      });
  });
  </script>
</body>
</html>