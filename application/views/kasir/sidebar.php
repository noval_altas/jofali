  <!-- ////////////////////////////////////////////////////////////////////////////-->
  <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="main-menu-content">
      <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
        <li class=" navigation-header">
          <span>General</span><i class=" ft-minus" data-toggle="tooltip" data-placement="right"
          data-original-title="General"></i>
        </li>

        <li class="nav-item active"><a href="<?= base_url('kasir/kasir')?>"><i class="ft-home"></i><span class="menu-title" data-i18n="">Dashboard</span></a>
        </li>
        <li class="nav-item"><a href="<?= base_url('kasir/kasir/pesan')?>"><i class="ft-calendar"></i><span class="menu-title" data-i18n="">Pesanan</span></a>
        </li>
        <li class="nav-item"><a href="<?= base_url('kasir/kasir/invoice')?>"><i class="ft-info"></i><span class="menu-title" data-i18n="">Invoice</span></a>
        <!-- <li class=" nav-item"><a href="index.html"><i class="ft-home"></i><span class="menu-title" data-i18n="">Dashboard</span><span class="badge badge badge-primary badge-pill float-right mr-2">3</span></a>
          <ul class="menu-content">
            <li class="active"><a class="menu-item" href="dashboard-ecommerce.html">eCommerce</a>
            </li>
            <li><a class="menu-item" href="dashboard-analytics.html">Analytics</a>
            </li>
            <li><a class="menu-item" href="dashboard-fitness.html">Fitness</a>
            </li>
          </ul>
        </li>
        <li class=" nav-item"><a href="email-application.html"><i class="ft-mail"></i><span class="menu-title" data-i18n="">Email Application</span></a>
        </li> -->
      </ul>
    </div>
  </div>