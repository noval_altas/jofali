<!DOCTYPE html>
<html lang="id">
<head>

  <!-- SITE TITTLE -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Jofali Coffee</title>
  
  <!-- FAVICON -->
  <link href="<?= base_url('assets/'); ?>images/logojofali.png" rel="shortcut icon">
  <!-- Bootstrap -->
  <link rel="stylesheet" href="<?= base_url('assets/'); ?>plugins/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/'); ?>plugins/bootstrap/css/bootstrap-slider.css">
  <!-- Font Awesome -->
  <link href="<?= base_url('assets/'); ?>plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- Owl Carousel -->
  <link href="<?= base_url('assets/'); ?>plugins/slick-carousel/slick/slick.css" rel="stylesheet">
  <link href="<?= base_url('assets/'); ?>plugins/slick-carousel/slick/slick-theme.css" rel="stylesheet">
  <!-- Fancy Box -->
  <link href="<?= base_url('assets/'); ?>plugins/fancybox/jquery.fancybox.pack.css" rel="stylesheet">
  <link href="<?= base_url('assets/'); ?>plugins/jquery-nice-select/css/nice-select.css" rel="stylesheet">
  <!-- CUSTOM CSS -->
  <link href="<?= base_url('assets/'); ?>css/style.css" rel="stylesheet">


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body class="body-wrapper">
	<section>
		<nav class="navbar navbar-expand-lg navbar-light navigation">
			<div class="container">
				<a class="navbar-brand" href="<?= base_url(); ?>">
				<img src="<?= base_url('assets/'); ?>images/logojofali.png" width="270 px" alt="45 px">
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav ml-auto main-nav ">
							<li class="nav-item active">
								<a class="nav-link" href="<?= base_url(); ?>"><font color="FFFFFF">Pesan Kopi</font></a>
							</li>

							<li class="nav-item active">
								<?php
								$keranjang = '<font color="FFFFFF"> Keranjang '.$this->cart->total_items(). ' Item </font>'
								?>
								<?php echo anchor('home/detailkeranjang/', $keranjang , array('class'=> "nav-link")); ?>
							</li>

						<div class="collapse navbar-collapse" id="navbarSupportedContent">
							<ul class="navbar-nav ml-auto main-nav ">
								<?php if ($this->session->userdata('username')) { ?>
									<li><a class="nav-link"><font color="FFFFFF">Selamat Datang <?= $this->session->userdata('nama')?></font></a></li>
									<li><a class="nav-link" href="<?= base_url('login/logout'); ?>"><font color="FFFFFF">Keluar</font></a></li>
								<?php } else { ?>
								<li class="nav-item active">
									<a class="nav-link" href="<?= base_url('login/auth'); ?>"><font color="FFFFFF">Masuk</font></a>
								</li>
								<?php } ?>
							</ul>
						</div>
					</ul>
				</div>
			</div>
		</div>
	</nav>
</section>

<!--===============================
=            Hero Area            =
================================-->

<section class="hero-area bg-1 text-center overly">
	<!-- Container Start -->
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- Header Contetnt -->
				<div class="content-block">
					<h2> <font color="white"> Kopi yang dibuat dengan cinta</font> </h2>				
				</div>
			</div>
		</div>
	</div>
	<!-- Container End -->
</section>

<!--===================================
=            Client Slider            =
====================================-->


<!--===========================================
=            Popular deals section            =
============================================-->

<section class="popular-deals section bg-gray">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section-title">
					<h2>Keranjang Belanja</h2>
					<p>Macam- Macam Pesanan Anda</p>
				</div>
			</div>
		</div>
		<div class="row">
			<!-- offer 01 -->
			<div class="col-lg-12">
				<div class="container-fluid">
					<div>
						<div class="alert alert-primary text-center" role="alert">
						  Total yang harus dibayar <strong> Rp <?= number_format($this->cart->total(), 0,',','.')?></strong>
						</div>
	                    <form method="post" action="<?= base_url('home/proses_pembayaran')?>">
	                        <fieldset class="p-4">
	                            <div class="form-group">
	                                <div class="row">
	                                    <div class="col-lg-6 py-2">
	                                        <input type="text" placeholder="Nama Lengkap*" class="form-control" name="nama" value="<?= $this->session->userdata('nama')?>" required>
	                                    </div>
	                                    <div class="col-lg-6 pt-2">
	                                        <input type="number" placeholder="No. Tlp *" class="form-control" name="notlp" required>
	                                    </div>
	                                </div>
	                            </div>
	                            <textarea class="form-control" placeholder="Pesan Khusus *" id="pesan" name="pesan" rows="8"></textarea>

	                            <div class="btn-grounp">
	                                <button type="submit" class="btn btn-primary mt-2 float-right">Proses...!</button>
	                            </div>
	                        </fieldset>
	                    </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!--============================
=            Footer            =
=============================-->

<footer class="footer-bottom">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-12">
      <div class="col-sm-6 col-12">
        <!-- Social Icons -->
        <ul class="social-media-icons text-right">
          <li><a class="fa fa-facebook" href="#" target="_blank"></a></li>
          <li><a class="fa fa-whatsapp" href="#" target="_blank"></a></li>
          <li><a class="fa fa-instagram" href="#" target="_blank"></a></li>
          <li><a class="fa fa-gmail" href="#"></a></li>
        </ul>
      </div>
    </div>
  </div>
</footer>

<!-- JAVASCRIPTS -->
<script src="<?= base_url('assets/'); ?>plugins/jQuery/jquery.min.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/bootstrap/js/popper.min.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/bootstrap/js/bootstrap-slider.js"></script>
  <!-- tether js -->
<script src="<?= base_url('assets/'); ?>plugins/tether/js/tether.min.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/raty/jquery.raty-fa.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/slick-carousel/slick/slick.min.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/jquery-nice-select/js/jquery.nice-select.min.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/fancybox/jquery.fancybox.pack.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/smoothscroll/SmoothScroll.min.js"></script>
<!-- google map -->
<script src="<?= base_url('assets/'); ?>plugins/google-map/gmap.js"></script>
<script src="<?= base_url('assets/'); ?>js/script.js"></script>

</body>

</html>