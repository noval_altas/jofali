<!DOCTYPE html>
<html lang="id">
<head>

  <!-- SITE TITTLE -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Jofali Coffee</title>
  
  <!-- FAVICON -->
  <link href="<?= base_url('assets/'); ?>images/logobawah.png" rel="shortcut icon">
  <!-- Bootstrap -->
  <link rel="stylesheet" href="<?= base_url('assets/'); ?>plugins/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/'); ?>plugins/bootstrap/css/bootstrap-slider.css">
  <!-- Font Awesome -->
  <link href="<?= base_url('assets/'); ?>plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- Owl Carousel -->
  <link href="<?= base_url('assets/'); ?>plugins/slick-carousel/slick/slick.css" rel="stylesheet">
  <link href="<?= base_url('assets/'); ?>plugins/slick-carousel/slick/slick-theme.css" rel="stylesheet">
  <!-- Fancy Box -->
  <link href="<?= base_url('assets/'); ?>plugins/fancybox/jquery.fancybox.pack.css" rel="stylesheet">
  <link href="<?= base_url('assets/'); ?>plugins/jquery-nice-select/css/nice-select.css" rel="stylesheet">
  <!-- CUSTOM CSS -->
  <link href="<?= base_url('assets/'); ?>css/style.css" rel="stylesheet">


  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body class="body-wrapper">
	<section>
		<nav class="navbar navbar-expand-lg navbar-light navigation">
			<div class="container">
				<a class="navbar-brand" href="<?= base_url(); ?>">
				<img src="<?= base_url('assets/'); ?>images/logojofali.png" width="270 px" alt="45 px">
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav ml-auto main-nav ">
							<li class="nav-item active">
								<a class="nav-link" href="<?= base_url('home'); ?>"><font color="FFFFFF">Pesan Kopi</font></a>
							</li>

							<li class="nav-item active">
								<?php
								$keranjang = '<font color="FFFFFF"> Keranjang '.$this->cart->total_items(). ' Item </font>'
								?>
								<?php echo anchor('home/detailkeranjang/', $keranjang , array('class'=> "nav-link")); ?>
							</li>

						<div class="collapse navbar-collapse" id="navbarSupportedContent">
							<ul class="navbar-nav ml-auto main-nav ">
								<?php if ($this->session->userdata('username')) { ?>
									<li><a class="nav-link"><font color="FFFFFF">Selamat Datang <?= $this->session->userdata('nama')?></font></a></li>
									<li><a class="nav-link" href="<?= base_url('login/logout'); ?>"><font color="FFFFFF">Keluar</font></a></li>
								<?php } else { ?>
								<li class="nav-item active">
									<a class="nav-link" href="<?= base_url('login'); ?>"><font color="FFFFFF">Masuk</font></a>
								</li>
								<?php } ?>

								

								<!-- <li class="nav-item dropdown dropdown-slide">
								<a class="nav-link dropdown-toggle" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<font color="FFFFFF">Tentang Kami</font> <span><i class="fa fa-angle-down"></i></span>
								</a>
								<div class="dropdown-menu">
									<a class="dropdown-item" href="#">Ad-Gird View</a>
									<a class="dropdown-item" href="#">Ad-List View</a>
								</div>
								</li> -->
							</ul>
						</div>
					</ul>
				</div>
			</div>
		</div>
	</nav>
</section>

<!--===============================
=            Hero Area            =
================================-->

<section class="hero-area bg-1 text-center overly">
	<!-- Container Start -->
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- Header Contetnt -->
				<div class="content-block">
					<h2> <font color="white"> Kopi yang dibuat dengan cinta</font> </h2>				
				</div>
			</div>
		</div>
	</div>
	<!-- Container End -->
</section>

<!--===================================
=            Client Slider            =
====================================-->


<!--===========================================
=            Popular deals section            =
============================================-->

<section class="popular-deals section bg-gray">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section-title">
					<h2>Kopi Terpopuler</h2>
					<p>macam - macam minuman yang terpopuler di kedai kami</p>
				</div>
			</div>
		</div>
		<div class="row">
			<!-- offer 01 -->
			<div class="col-lg-12">
				<div class="trending-ads-slide">
					<?php foreach ($menu as $value) { ?>
						<!-- <div class="col-sm-12 col-lg-4">
							<div class="card">
							  <img class="card-img-top img-fluid" src="<?= base_url(); ?>assets/images/jofali/<?= $value->fotomenu; ?>" weight="1px">
							  <div class="card-body">
							    <h5 class="card-title"><?= $value->namamenu; ?></h5>
							    <p class="card-text"><?= 'Rp '.number_format($value->hargamenu); ?></p>
							    <div class="text-center">
							  	<a href="<?= base_url('menu/create'); ?>"><button type="button" class="btn btn-primary btn-min-width" data-toggle="tooltip" data-placement="left" title="Tambah Daftar Menu"><i class="fa fa-plus"></i> Tambah Daftar Menu</button></a>
							  </div>
							  </div>
							</div>
							<div class="card">
			                    <div class="card-content">
			                      <img class="card-img-top img-fluid" src="<?= base_url(); ?>assets/images/jofali/<?= $value->fotomenu; ?>" alt="Card image cap" />
			                      <span class="badge badge-pill badge-primary">Branding</span>
			                      <div class="card-body">
			                        <h4 class="card-title"><?= $value->namamenu; ?></h4>
			                        <p class="card-text">This card has supporting text below as a natural lead-in
			                          to additional content.</p>
			                        <p class="card-text">
			                          <small class="text-muted">Last updated 3 mins ago</small>
			                          <span class="text-right">
			                          
			                          </span>
			                        </p>
			                      </div>
			                    </div>
			                  </div>
						</div> -->
						<div class="col-xl-4 col-md-6">
			              <div class="card">
			                <div class="card-content">
			                  <div class="card-body text-center">
			                    <h5><?= $value->namamenu; ?></h5>
			                  </div>
			                  <img class="img-fluid" src="<?= base_url(); ?>assets/images/jofali/<?= $value->fotomenu; ?>" alt="Card image cap">
			                  <div class="card-body text-center">
			                    <p><?= 'Rp '.number_format($value->hargamenu, 0,',','.'); ?></p>
			                    <a href="<?= base_url('home/tambahkeranjang/'.$value->idmenu); ?>"><button type="button" class="btn btn-primary btn-min-width" data-toggle="tooltip" data-placement="left" title="Tambah Ke Keranjang"><i class="fa fa-shopping-cart"></i></button></a>
			                    <a href="<?= base_url(''); ?>"><button type="button" class="btn btn-primary btn-min-width" data-toggle="tooltip" data-placement="left" title="Info"><i class="fa fa-info-circle"></i></button></a>
			                  </div>
			                </div>
			              </div>
			            </div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>

<!--============================
=            Footer            =
=============================-->

<footer class="footer-bottom">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-12">
      <div class="col-sm-6 col-12">
        <!-- Social Icons -->
        <ul class="social-media-icons text-right">
          <li><a class="fa fa-facebook" href="#" target="_blank"></a></li>
          <li><a class="fa fa-whatsapp" href="#" target="_blank"></a></li>
          <li><a class="fa fa-instagram" href="#" target="_blank"></a></li>
          <li><a class="fa fa-gmail" href="#"></a></li>
        </ul>
      </div>
    </div>
  </div>
</footer>

<!-- JAVASCRIPTS -->
<script src="<?= base_url('assets/'); ?>plugins/jQuery/jquery.min.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/bootstrap/js/popper.min.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/bootstrap/js/bootstrap-slider.js"></script>
  <!-- tether js -->
<script src="<?= base_url('assets/'); ?>plugins/tether/js/tether.min.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/raty/jquery.raty-fa.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/slick-carousel/slick/slick.min.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/jquery-nice-select/js/jquery.nice-select.min.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/fancybox/jquery.fancybox.pack.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/smoothscroll/SmoothScroll.min.js"></script>
<!-- google map -->
<script src="<?= base_url('assets/'); ?>plugins/google-map/gmap.js"></script>
<script src="<?= base_url('assets/'); ?>js/script.js"></script>

</body>

</html>