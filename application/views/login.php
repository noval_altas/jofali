<!DOCTYPE html>
<html lang="id">
<head>

  <!-- SITE TITTLE -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Jofali Coffee</title>
  
  <!-- FAVICON -->
  <link href="<?= base_url('assets/'); ?>images/logobawah.png" rel="shortcut icon">
  <!-- PLUGINS CSS STYLE -->
  <link href="<?= base_url('assets/');?>plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
  <!-- Bootstrap -->
  <link rel="stylesheet" href="<?= base_url('assets/'); ?>plugins/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url('assets/'); ?>plugins/bootstrap/css/bootstrap-slider.css">
  <!-- Font Awesome -->
  <link href="<?= base_url('assets/'); ?>plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <!-- Owl Carousel -->
  <link href="<?= base_url('assets/'); ?>plugins/slick-carousel/slick/slick.css" rel="stylesheet">
  <link href="<?= base_url('assets/'); ?>plugins/slick-carousel/slick/slick-theme.css" rel="stylesheet">
  <!-- Fancy Box -->
  <link href="<?= base_url('assets/'); ?>plugins/fancybox/jquery.fancybox.pack.css" rel="stylesheet">
  <link href="<?= base_url('assets/'); ?>plugins/jquery-nice-select/css/nice-select.css" rel="stylesheet">
  <!-- CUSTOM CSS -->
 <link href="<?= base_url('assets/'); ?>css/style.css" rel="stylesheet">

</head>

<body class="body-wrapper">

<section>
  <nav class="navbar navbar-expand-lg navbar-light navigation">
    <div class="container">
      <a class="navbar-brand" href="<?= base_url(); ?>">
        <img src="<?= base_url('assets/'); ?>images/logojofali.png" width="270 px" alt="45 px">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto main-nav ">
          <li class="nav-item active">
            <a class="nav-link" href="<?= base_url(); ?>"><font color="FFFFFF">Pesan Kopi</font></a>
          </li>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto main-nav ">
              <li class="nav-item active">
                <a class="nav-link" href=""><font color="FFFFFF">Pesan Kopi</font></a>
              </li>
              <li class="nav-item dropdown dropdown-slide">
                <a class="nav-link dropdown-toggle" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <font color="FFFFFF">Tentang Kami</font> <span><i class="fa fa-angle-down"></i></span>
                </a>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="#">Ad-Gird View</a>
                  <a class="dropdown-item" href="#">Ad-List View</a>
                </div>
              </li>
            </ul>
          </div>
        </ul>
      </div>
    </div>
  </nav>
</section>
<body bgcolor="8B6B65">
<section class="login py-5 border-top-1">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-8 align-item-center">
                <div class="border">
                  <center><h2 class="px-4 mt-3"><font color="8B6B65">Masuk Sekarang</font></h2></center>
                    <center><h6 class="px-4"><font color="FFFFF">Hai! Silahkan Memulai <br><br><?= $this->session->flashdata('pesan'); ?></font></h6></center>
                    
                    <form method="post" action="<?= base_url('login'); ?>">
                        <fieldset class="p-4">
                            <input type="text" name="username" id="username" placeholder="username" class="border p-3 w-100 my-2 bg-white">
                            <?= form_error('username','<div class="text-danger small ml-2">','</div>')?>

                            <input type="password" name="password" id="password" placeholder="Password" class="border p-3 w-100 my-2 bg-white">
                            <?= form_error('password','<div class="text-danger small ml-2">','</div>')?>
                            
                            <center><button type="submit" class="d-block py-3 px-5 bg-primary text-white border-0 rounded font-weight-bold mt-3">Masuk</button></center>

                            <a class="mt-3 d-block  text-primary" href="#"> <font color="FFFFF"> Lupa Kata Sandi ?</font></a>

                            <a class="mt-3 d-inline-block text-primary" href="<?= base_url('Login/regis'); ?>"><font color="FFFFF">Belum punya akun ? Daftar</font></a>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
</body>

<!--============================
=            Footer            =
=============================-->

  <!-- Container End -->
<!-- Footer Bottom -->
<footer class="footer-bottom">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-12">
      <div class="col-sm-6 col-12">
        <!-- Social Icons -->
        <ul class="social-media-icons text-right">
          <li><a class="fa fa-facebook" href="#" target="_blank"></a></li>
          <li><a class="fa fa-whatsapp" href="#" target="_blank"></a></li>
          <li><a class="fa fa-instagram" href="#" target="_blank"></a></li>
          <li><a class="fa fa-gmail" href="#"></a></li>
        </ul>
      </div>
    </div>
  </div>
</footer>
  
<!-- JAVASCRIPTS -->
<script src="<?= base_url('assets/'); ?>plugins/jQuery/jquery.min.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/bootstrap/js/popper.min.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/bootstrap/js/bootstrap-slider.js"></script>
  <!-- tether js -->
<script src="<?= base_url('assets/'); ?>plugins/tether/js/tether.min.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/raty/jquery.raty-fa.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/slick-carousel/slick/slick.min.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/jquery-nice-select/js/jquery.nice-select.min.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/fancybox/jquery.fancybox.pack.js"></script>
<script src="<?= base_url('assets/'); ?>plugins/smoothscroll/SmoothScroll.min.js"></script>
<!-- google map -->
<script src="<?= base_url('assets/'); ?>plugins/google-map/gmap.js"></script>
<script src="<?= base_url('assets/'); ?>js/script.js"></script>

</body>

</html>