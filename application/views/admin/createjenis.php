  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
      	<div class="row">
      		<div class="col-xl-6 col-lg-12">
              <div class="card">
                <div class="card-header">
                  <label class="card-title" for="iconLeft4">Tambah Data Jenis Varian</label>
                </div>
                <div class="card-content">
                  <div class="card-body">
                  <?= $this->session->flashdata('pesan'); ?>
                    <form method="post" enctype="multipart/form-data" action="<?= base_url('admin/varian/save'); ?>">

                    <p>Jenis Varian</p>
                    <fieldset class="form-group position-relative has-icon-left">
                      <input type="text" name="jenismenu" id="jenismenu" class="form-control" id="iconLeft4" placeholder="Masukkan Jenis Varian" value="<?= set_value('jenismenu'); ?>">
                      <?= form_error('jenismenu','<small class="text-danger">', '</small>'); ?>
                      <div class="form-control-position">
                        <i class="ft-book primary"></i>
                      </div>
                    </fieldset>

                    <fieldset>
                    	<div class="text-right">
                          <button type="submit" class="btn btn-primary" data-toggle="tooltip" data-placement="left" title="Tambah Jenis Varian"><i class="fa fa-plus"></i> Tambah Jenis Varian</button>
                        </div>
                    </fieldset>

                    </form>
                  </div>
                </div>
              </div>
            </div>
      	</div>
      </div>
    </div>
  </div>