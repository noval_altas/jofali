  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
      	<div class="row">
      		<div class="col-xl-6 col-lg-12">
              <div class="card">
                <div class="card-header">
                  <label class="card-title" for="iconLeft4">Edit Data Menu</label>
                </div>
                <div class="card-content">
                  <div class="card-body">
                  <?= $this->session->flashdata('pesan'); ?>
                    <form method="post" enctype="multipart/form-data" action="<?= base_url('admin/menu/save'); ?>">

                    <p>Nama Menu</p>
                    <fieldset class="form-group position-relative has-icon-left">
                      <input type="text" name="namamenu" id="namamenu" class="form-control" id="iconLeft4" placeholder="Masukkan Nama Menu" value="<?= $menu->namamenu ?>">
                      <?= form_error('namamenu','<small class="text-danger">', '</small>'); ?>
                      <div class="form-control-position">
                        <i class="ft-book primary"></i>
                      </div>
                    </fieldset>

                    <p>Jenis Menu</p>
                    <fieldset class="form-group position-relative has-icon-left">
                        <select class="form-control" name="jenismenu" id="basicSelect" placeholder="Pilih Jenis Menu">
                          <?php foreach($jenismenu as $jm) { ?>
                          <option value="<?= $jm->idjenismenu; ?>" <?php if($jm->idjenismenu==$menu->idjenismenu) echo 'selected="selected"'; ?>><?= $jm->namajenis?></option>
                          <?php } ?>
                        </select>
                        <div class="form-control-position">
                        <i class="ft-list primary"></i>
                      </div>
                    </fieldset>

                    <p>Harga Menu</p>
                    <fieldset class="form-group position-relative has-icon-left">
                      <input type="text" name="hargamenu" id="hargamenu" class="form-control" id="iconLeft4" placeholder="Masukkan Nominal Harga" value="<?= $menu->hargamenu; ?>">
                      <?= form_error('hargamenu','<small class="text-danger">', '</small>'); ?>
                      <div class="form-control-position">
                        <i class="fa fa-money primary"></i>
                      </div>
                    </fieldset>

                    <fieldset class="form-group">
                      	<label for="basicInputFile">Foto Menu</label><br>

                        <img src="<?= base_url(); ?>assets/images/jofali/<?= $menu->fotomenu; ?>" weight="100px" height="100px">
                      	<input type="file" name="fotomenu" id="fotomenu" class="form-control-file mt-1" id="basicInputFile" value="<?= $menu->fotomenu; ?>">
                      	<?= form_error('fotomenu','<small class="text-danger">', '</small>'); ?>
                        <br>
                    </fieldset>

                    <fieldset>
                    	<div class="text-right">
                          <button type="submit" class="btn btn-primary" data-toggle="tooltip" data-placement="left" title="Edit Menu"><i class="fa fa-plus"></i> Edit Menu</button>
                        </div>
                    </fieldset>

                    </form>
                  </div>
                </div>
              </div>
            </div>
      	</div>
      </div>
    </div>
  </div>