  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <!-- Default ordering table -->
        <section id="ordering">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Invoice Pemesanan Produk</h4>
                  <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <table class="table table-striped table-bordered default-ordering">
                      <thead>
                        <tr>
                          <th class="text-center">Nama Pemesan</th>
                          <th class="text-center">Alamat</th>
                          <th class="text-center">No. Tlp</th>
                          <th class="text-center">Pesan</th>
                          <th class="text-center">Total Harga</th>
                          <th class="text-center">Tgl. Pesan</th>
                          <th class="text-center">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($invoice as $inv) { ?>
                        <tr>
                          <td class="text-center"><?= $inv->nama; ?></td>
                          <td class="text-center"><?= $inv->alamat; ?></td>
                          <td class="text-center"><?= $inv->notlp; ?></td>
                          <td class="text-center"><?= $inv->pesan;?><?= status_produk($inv->aksi); ?></td>
                          <td class="text-center">Rp <?= number_format($inv->hargatotal, 0,',','.'); ?></td>
                          <td class="text-center"><?= $inv->tglpesan; ?></td>
                          <td class="text-center">
                            <a href="<?= base_url('admin/invoice/detinvoice/'.$inv->idinvoice); ?>"><button type="button" class="btn btn-info" data-toggle="tooltip" data-placement="left" title="Detail Invoice"><i class="fa fa-info"></i></button></a>
                          </td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--/ Default ordering table -->
      </div>
    </div>
  </div>