  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <!-- Form wizard with step validation section start -->
        <section id="validation">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Form Pemesanan</h4>
                  <a class="heading-elements-toggle"><i class="fa fa-ellipsis-h font-medium-3"></i></a>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                    <form action="#" class="steps-validation wizard-circle">
                      <!-- Step 1 -->
                      <h6>Data</h6>
                      <fieldset>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="firstName3">
                                Nama Pelanggan :
                                <span class="danger">*</span>
                              </label>
                              <input type="text" class="form-control required" id="nama" name="nama">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="lastName3">
                                Kasir :
                              </label>
                              <input type="text" class="form-control" value="<?= $this->session->userdata('username')?>" id="lastName3" name="lastName" disabled="disabled">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="emailAddress5">
                                Email :
                              </label>
                              <input type="email" name="email" class="form-control" id="email">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="date4">Tanggal Pemesanan :</label>
                              <input type="text" class="form-control" value="<?php echo date('d M Y'); ?>" disabled="disabled">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="phoneNumber3">No. Tlp :</label>
                              <input type="tel" class="form-control" id="phoneNumber3">
                            </div>
                          </div>
                        </div>
                      </fieldset>
                      <!-- Step 2 -->
                      <h6>Pesanan</h6>
                      <fieldset>
                        <div class="row">
                          <div class="col-md-7">
                            <div class="form-group">
                            <label>Pilih Menu</label>
                            <fieldset class="form-group position-relative has-icon-left">
                                <select class="form-control" name="menu" id="basicSelect" placeholder="Pilih Jenis Menu">
                                  <?php foreach($menu as $m) { ?>
                                  <option value="<?= $m->idmenu; ?>"><?= $m->namamenu?></option>
                                  <?php } ?>
                                </select>
                                <div class="form-control-position">
                                <i class="ft-list primary"></i>
                              </div>
                            </fieldset>
                            </div>
                          </div>
                          <div class="col-md-3">
                            <div class="form-group">
                              <label>
                                Jumlah :
                              </label>
                              <input type="number" class="form-control" id="Jumlah" name="Jumlah">
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="form-group">
                              <label>
                              </label>
                              <button class="btn btn-success" style="width: 100%;"><i class="fa fa-plus position-left"></i> Tambah Menu</button>
                            </div>
                          </div>
                        </div>
                      </fieldset>
                      <!-- Step 3 -->
                      <h6>Pembayaran</h6>
                      <fieldset>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="eventName3">
                                Event Name :
                                <span class="danger">*</span>
                              </label>
                              <input type="text" class="form-control required" id="eventName3" name="eventName">
                            </div>
                            <div class="form-group">
                              <label for="eventType3">
                                Event Type :
                                <span class="danger">*</span>
                              </label>
                              <select class="custom-select form-control required" id="eventType3" name="eventType">
                                <option value="Banquet">Banquet</option>
                                <option value="Fund Raiser">Fund Raiser</option>
                                <option value="Dinner Party">Dinner Party</option>
                                <option value="Wedding">Wedding</option>
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="eventLocation3">Event Location :</label>
                              <select class="custom-select form-control" id="eventLocation3" name="eventLocation">
                                <option value="">Select City</option>
                                <option value="Amsterdam">Amsterdam</option>
                                <option value="Berlin">Berlin</option>
                                <option value="Frankfurt">Frankfurt</option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="eventDate">
                                Event Date - Time :
                                <span class="danger">*</span>
                              </label>
                              <div class='input-group'>
                                <input type='text' class="form-control datetime required" id="eventDate" name="eventDate"
                                />
                                <div class="input-group-append">
                                  <span class="input-group-text">
                                    <span class="ft-calendar"></span>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="eventStatus3">
                                Event Status :
                                <span class="danger">*</span>
                              </label>
                              <select class="custom-select form-control required" id="eventStatus3" name="eventStatus">
                                <option value="Planning">Planning</option>
                                <option value="In Progress">In Progress</option>
                                <option value="Finished">Finished</option>
                              </select>
                            </div>
                            <div class="form-group">
                              <label>Requirements :</label>
                              <div class="c-inputs-stacked">
                                <div class="d-inline-block custom-control custom-checkbox">
                                  <input type="checkbox" name="status3" class="custom-control-input" id="staffing3">
                                  <label class="custom-control-label" for="staffing3">Staffing</label>
                                </div>
                                <div class="d-inline-block custom-control custom-checkbox">
                                  <input type="checkbox" name="status3" class="custom-control-input" id="catering3">
                                  <label class="custom-control-label" for="catering3">Catering</label>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </fieldset>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- Form wizard with step validation section end -->
      </div>
    </div>
  </div>