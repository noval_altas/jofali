  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <!-- Default ordering table -->
        <section id="ordering">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Detail Pemesanan Produk</h4>
                  <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    
                    
                    <table class="table table-striped table-bordered default-ordering">
                      <thead>
                        <tr>
                          <th class="text-center">No</th>
                          <th class="text-center">Nama Produk</th>
                          <th class="text-center">Jumlah Pesanan</th>
                          <th class="text-center">Harga Satuan</th>
                          <th class="text-center">Sub-Total</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $no = 1;
                        $total = 0;
                        foreach ($pesanan as $psn) {
                          $subtotal = $psn->jumlah * $psn->harga;
                          $total += $subtotal;
                        ?>
                          <tr>
                            <td class="text-center"><?= $no++; ?></td>
                            <td class="text-center"><?= $psn->nama_brg; ?></td>
                            <td class="text-center"><?= $psn->jumlah; ?></td>
                            <td class="text-center">Rp <?= number_format($psn->harga,0,',','.'); ?></td>
                            <td class="text-center">Rp <?= number_format($subtotal,0,',','.'); ?></td>
                          </tr>
                        <?php } ?>
                          <tr>
                            <td colspan="4" align="right">Sub Total Produk</td>
                            <td class="text-center">Rp <?= number_format($total,0,',','.'); ?></td>
                          </tr>
                          <?php foreach ($invoice as $inv) { 
                            if ($inv->aksi == 'ambil') {
                          ?>
                          <tr>
                            <td colspan="4" align="right">Ongkir</td>
                            <td class="text-center">Rp <?= number_format(0 ,0,',','.'); ?></td>
                          </tr>
                          <tr>
                            <td colspan="4" align="right">Grand Total</td>
                            <td class="text-center">Rp <?= number_format($total,0,',','.'); ?></td>
                          </tr>
                          <?php } else { ?>
                            <tr>
                              <td colspan="4" align="right">Ongkir</td>
                              <td class="text-center">Rp <?= number_format(5000 ,0,',','.'); ?></td>
                            </tr>
                            <tr>
                              <td colspan="4" align="right">Grand Total</td>
                              <td class="text-center">Rp <?= number_format($total+5000,0,',','.'); ?></td>
                            </tr>
                          <?php } ?>
                          <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--/ Default ordering table -->
      </div>
    </div>
  </div>