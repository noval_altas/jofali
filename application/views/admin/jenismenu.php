  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <!-- Default ordering table -->
        <section id="ordering">
          <div class="row">
            <div class="col-6">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Daftar Jenis</h4>
                  <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li>
                        <a href="<?= base_url('admin/varian/create'); ?>"><button type="button" class="btn btn-primary btn-min-width" data-toggle="tooltip" data-placement="left" title="Tambah Daftar Menu"><i class="fa fa-plus"></i> Tambah Daftar Jenis</button></a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="flash-data" data-flashData="<?= $this->session->flashdata('pesan'); ?>"></div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <table class="table table-striped table-bordered default-ordering">
                      <thead>
                        <tr>
                          <th class="text-center">Jenis</th>
                          <th class="text-center">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($varian as $v) {?>
                        <tr>
                          <td class="text-center"><?= $v->namajenis; ?></td>
                          <td class="text-center">
                            <a href="<?= base_url('admin/varian/edit/'.$v->idjenismenu); ?>"><button type="button" class="btn btn-info" data-toggle="tooltip" data-placement="left" title="Edit Varian"><i class="fa fa-pencil"></i></button></a>
                            <!-- <a href="<?= base_url('varian/delete/'.$v->idjenismenu); ?>"><button type="submit" class="btn btn-warning removejenis" data-toggle="tooltip" data-placement="left" title="Hapus Varian"><i class="fa fa-trash-o"></i></button></a> -->
                            <button type="submit" class="btn btn-warning removejenis" data-href="<?= base_url('admin/varian/delete/'.$v->idjenismenu); ?>" data-toggle="tooltip" data-placement="left" title="Hapus Varian"><i class="fa fa-trash-o"></i></button>
                          </td>
                        </tr>
                        <?php } ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <th class="text-center">Jenis</th>
                          <th class="text-center">Aksi</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--/ Default ordering table -->
      </div>
    </div>
  </div>
