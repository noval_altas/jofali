  <div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
      </div>
      <div class="content-body">
        <!-- Default ordering table -->
        <section id="ordering">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title">Daftar Menu</h4>
                  <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li>
                        <a href="<?= base_url('admin/menu/create'); ?>"><button type="button" class="btn btn-primary btn-min-width" data-toggle="tooltip" data-placement="left" title="Tambah Daftar Menu"><i class="fa fa-plus"></i> Tambah Daftar Menu</button></a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body card-dashboard">
                    <table class="table table-striped table-bordered default-ordering">
                      <thead>
                        <tr>
                          <th>Foto Menu</th>
                          <th>Nama Menu</th>
                          <th>Jenis</th>
                          <th>Harga</th>
                          <th class="text-center">Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($menu as $m) {?>
                        <tr>
                          <td class="text-center"><img src="<?= base_url(); ?>assets/images/jofali/<?= $m->fotomenu; ?>" weight="50px" height="50px"></td>
                          <td class="text-center"><?= $m->namamenu; ?></td>
                          <td class="text-center"><?= $m->namajenis; ?></td>
                          <td class="text-center"><h5><span class="badge badge-info"><?= 'Rp '.number_format($m->hargamenu); ?></span></h5></td>
                          <td class="text-center">
                            <a href="<?= base_url('admin/menu/edit/'.$m->idmenu); ?>"><button type="button" class="btn btn-info" data-toggle="tooltip" data-placement="left" title="Edit Menu"><i class="fa fa-pencil"></i></button></a>
                            <a href="<?= base_url('admin/menu/delete/'.$m->idmenu); ?>"><button type="button" class="btn btn-warning" data-toggle="tooltip" data-placement="left" title="Hapus Menu"><i class="fa fa-trash-o"></i></button></a>
                          </td>
                        </tr>
                        <?php } ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <th>Foto Menu</th>
                          <th>Nama Menu</th>
                          <th>Jenis</th>
                          <th>Harga</th>
                          <th class="text-center">Aksi</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--/ Default ordering table -->
      </div>
    </div>
  </div>