<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$data['menu'] = $this->HomeModel->tampil();
		$this->load->view('home/index',$data);
	}

	public function tambahkeranjang($id)
	{
		$menu = $this->HomeModel->find($id);

		$data = array(
			'id'	=> $menu->idmenu,
			'qty'	=> 1,
			'price'	=> $menu->hargamenu,
			'name'	=> $menu->namamenu

		);

		$this->cart->insert($data);
		redirect('home');
	}

	public function detailkeranjang()
	{
		$this->load->view('home/keranjang');
	}

	public function hapuskeranjang()
	{
		$this->cart->destroy();
		redirect('home');
	}

	public function pembayaran()
	{
		if ($this->cart->total() > 1) {
			$this->load->view('home/ambilsendiri');
		} else {
			redirect('home');
		}
	}

	public function kirim()
	{
		$data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
		if ($this->cart->total() > 1) {
			$this->load->view('home/dikirim', $data);
		} else {
			redirect('home');
		}
	}	

	public function proses_pembayaran1()
	{
		$proses = $this->InvoiceModel->dikirim();
		if ($proses) {
			$this->cart->destroy();
			$this->load->view('home/berhasil');
		} else {
			echo "Maaf pesanan anda gagal diproses...!!!";
		}
	}

	public function proses_pembayaran()
	{
		$proses = $this->InvoiceModel->index();
		if ($proses) {
			$this->cart->destroy();
			$this->load->view('home/berhasil');
		} else {
			echo "Maaf pesanan anda gagal diproses...!!!";
		}
	}

}