<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kasir extends CI_Controller {

	public function __construct(){
        parent::__construct();
        if(!$this->session->userdata('username')){
            redirect('Auth/login');
        }
    }

	public function index()
	{	
		$data ['user'] = $this->AuthModel->datauser();
		$data ['title'] = "Dashboard";
		if ($data['user']['hakakses'] == 3) {
			$data['countproduk'] = $this->AdminModel->countproduk();
			$data['countuser'] = $this->AdminModel->countuser();
			$data['counttotal'] = $this->AdminModel->counttotal();
			$data['countharga'] = $this->AdminModel->countharga();
			$this->load->view('template_admin/header', $data);
			$this->load->view('template_admin/navbar');
			$this->load->view('kasir/sidebar');
			$this->load->view('kasir/index');
			$this->load->view('template_admin/footer');
		} else {
			redirect('home');
		}
	}

	public function pesan()
	{
		$data ['menu'] = $this->MenuModel->menu();
		$data ['user'] = $this->AuthModel->datauser();
		$data ['title'] = "Pesan";
		if ($data['user']['hakakses'] == 3) {
			$this->load->view('template_admin/header', $data);
			$this->load->view('template_admin/navbar');
			$this->load->view('kasir/sidebar');
			$this->load->view('admin/pesan', $data);
			$this->load->view('template_admin/footer');
		} else {
			redirect('home');
		}
	}

	public function invoice()
	{
		$data ['invoice'] = $this->InvoiceModel->tampil_data();
		$data ['user'] = $this->AuthModel->datauser();
		$data ['title'] = "Invoice";
			$this->load->view('template_admin/header', $data);
			$this->load->view('template_admin/navbar');
			$this->load->view('kasir/sidebar');
			$this->load->view('admin/invoice.php', $data);
			$this->load->view('template_admin/footer');
	}
}