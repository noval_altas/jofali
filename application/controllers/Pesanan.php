<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesanan extends CI_Controller {

	public function __construct(){
        parent::__construct();
        if(!$this->session->userdata('username')){
            redirect('login');
        }
    }

	public function index()
	{	
		$data ['user'] = $this->AuthModel->datauser();
		$data ['title'] = "Pesanan";
		if ($data['user']['hakakses'] == 1||3) {
			$this->load->view('template_admin/header', $data);
			$this->load->view('template_admin/navbar');
			$this->load->view('template_admin/sidebar');
			$this->load->view('admin/pesan');
			$this->load->view('template_admin/footer');
		} else {
			redirect('home');
		}
		
	}
}