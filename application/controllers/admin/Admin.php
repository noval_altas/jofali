<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct(){
        parent::__construct();
        if(!$this->session->userdata('username')){
            redirect('login');
        }
    }

	public function index()
	{	
		$data ['user'] = $this->AuthModel->datauser();
		$data ['title'] = "Dashboard";
		if ($data['user']['hakakses'] == 1) {
			$data['countproduk'] = $this->AdminModel->countproduk();
			$data['countuser'] = $this->AdminModel->countuser();
			$data['counttotal'] = $this->AdminModel->counttotal();
			$data['countharga'] = $this->AdminModel->countharga();
			$this->load->view('template_admin/header', $data);
			$this->load->view('template_admin/navbar');
			$this->load->view('admin/sidebar');
			$this->load->view('admin/index');
			$this->load->view('template_admin/footer');
		} else {
			redirect('home');
		}
	}
}