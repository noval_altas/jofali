<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesan extends CI_Controller {

	public function __construct(){
        parent::__construct();
        if(!$this->session->userdata('username')){
            redirect('login');
        }
    }

	public function index()
	{	
		$data ['menu'] = $this->MenuModel->menu();
		$data ['user'] = $this->AuthModel->datauser();
		$data ['title'] = "Pesan";
		if ($data['user']['hakakses'] == 1) {
			$this->load->view('template_admin/header', $data);
			$this->load->view('template_admin/navbar');
			$this->load->view('admin/sidebar');
			$this->load->view('admin/pesan', $data);
			$this->load->view('template_admin/footer');
		} else {
			redirect('home');
		}
		
	}
}