<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data ['title'] = "Daftar Menu";
		$data ['menu'] = $this->MenuModel->menu();
		$this->load->view('template_admin/header', $data);
		$this->load->view('template_admin/navbar');
		$this->load->view('admin/sidebar');
		$this->load->view('admin/menu');
		$this->load->view('template_admin/footer');
	}

	public function create()
	{
		$data ['title'] = "Daftar Menu";
		$data ['jenismenu'] = $this->MenuModel->jenismenu();
		$this->load->view('template_admin/header', $data);
		$this->load->view('template_admin/navbar');
		$this->load->view('admin/sidebar');
		$this->load->view('admin/createmenu', $data);
		$this->load->view('template_admin/footer');
	}

	public function save()
	{
		$this->form_validation->set_rules('namamenu', 'Nama Menu', 'required|trim',[
			'required' => 'Form nama menu tidak boleh kosong!'
		]);
		$this->form_validation->set_rules('jenismenu', 'Jenis Menu', 'required|trim',[
			'required' => 'Form jenis menu tidak boleh kosong!'
		]);
		$this->form_validation->set_rules('hargamenu', 'Harga Menu', 'required|trim',[
			'required' => 'Form harga menu tidak boleh kosong!'
		]);

		if ($this->form_validation->run() == false) {
			$data ['title'] = "Daftar Menu";
			$data ['jenismenu'] = $this->MenuModel->jenismenu();
			$this->load->view('template_admin/header', $data);
			$this->load->view('template_admin/navbar');
			$this->load->view('admin/sidebar');
			$this->load->view('admin/createmenu', $data);
			$this->load->view('template_admin/footer');
		} else {
			$namamenu = htmlspecialchars($this->input->post('namamenu', true));
			$jenismenu = htmlspecialchars($this->input->post('jenismenu', true));
			$hargamenu = htmlspecialchars($this->input->post('hargamenu', true));
			$fotomenu = $_FILES['fotomenu'];
				if ($fotomenu = '')
				{
					echo "Gamabar tidak ada";
				} else {
					$config ['upload_path'] = './assets/images/jofali';
					$config	['allowed_types'] = 'jpg|jpeg|png|gif';

					$this->load->library('upload', $config);
					if (!$this->upload->do_upload('fotomenu')) {
						echo "Gambar gagal diupload!!";
					} else {
						$fotomenu = $this->upload->data('file_name');
					}
				}

			$data = array(
				'namamenu' => $namamenu,
				'hargamenu' => $hargamenu,
				'fotomenu' => $fotomenu,
				'idjenismenu' => $jenismenu
			);

			$this->MenuModel->insertmenu($data);
			$this->session->set_flashdata('pesan','<div class="alert bg-success alert-icon-left alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Congratulation!</strong> Your account has been created. Please Login...</div>');
			redirect('menu');
		}
	}

	public function edit($idmenu)
	{
		$data ['title'] = "Daftar Menu";
		$data ['jenismenu'] = $this->MenuModel->jenismenu();
		$data ['menu'] = $this->MenuModel->editmenu($idmenu);
		$this->load->view('template_admin/header', $data);
		$this->load->view('template_admin/navbar');
		$this->load->view('admin/sidebar');
		$this->load->view('admin/editmenu', $data);
		$this->load->view('template_admin/footer');
	}

	public function delete($id)
	{
		$where = array('idmenu' => $id);
		$this->VarianModel->delete($where, 'menu');
		$this->session->set_flashdata('pesan','dihapus');
		redirect('menu');
	}
}