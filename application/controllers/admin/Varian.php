<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Varian extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data ['title'] = "Jenis Varian";
		$data ['varian'] = $this->VarianModel->varian();
		$this->load->view('template_admin/header', $data);
		$this->load->view('template_admin/navbar');
		$this->load->view('admin/sidebar');
		$this->load->view('admin/jenismenu',$data);
		$this->load->view('template_admin/footer');
	}

	public function create()
	{
		$data ['title'] = "Jenis Varian";
		$this->load->view('template_admin/header', $data);
		$this->load->view('template_admin/navbar');
		$this->load->view('admin/sidebar');
		$this->load->view('admin/createjenis');
		$this->load->view('template_admin/footer');
	}

	public function save()
	{
		$this->form_validation->set_rules('jenismenu', 'Jenis Menu', 'required|trim',[
			'required' => 'Form jenis menu tidak boleh kosong!'
		]);

		if ($this->form_validation->run() == false) {
			$data ['title'] = "Jenis Varian";
			$this->load->view('template_admin/header', $data);
			$this->load->view('template_admin/navbar');
			$this->load->view('admin/sidebar');
			$this->load->view('admin/editjenis');
			$this->load->view('template_admin/footer');
		} else {
			$data = [
				'namajenis' => htmlspecialchars($this->input->post('jenismenu', true))
			];

			$this->VarianModel->insertjenis($data,'jenismenu');
			$this->session->set_flashdata('pesan','ditambah');
			// $this->session->set_flashdata('pesan','<div class="alert bg-success alert-icon-left alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Congratulation!</strong> Data Telah Disimpan.</div>');
			redirect('varian');
		}
	}

	public function edit($id)
	{
		$data ['title'] = "Jenis Varian";
		$where = array('idjenismenu' => $id);
		$data ['jenis'] = $this->VarianModel->edit($where, 'jenismenu')->result();
		$this->load->view('template_admin/header', $data);
		$this->load->view('template_admin/navbar');
		$this->load->view('admin/sidebar');
		$this->load->view('admin/editjenis', $data);
		$this->load->view('template_admin/footer');
	}

	public function update()
	{
		$this->form_validation->set_rules('jenismenu', 'Jenis Menu', 'required|trim',[
			'required' => 'Form jenis menu tidak boleh kosong!'
		]);

		if ($this->form_validation->run() == false) {
			$data ['title'] = "Jenis Varian";
			$where = array('idjenismenu' => $id);
			$data ['jenis'] = $this->VarianModel->edit($where, 'jenismenu')->result();
			$this->load->view('template_admin/header', $data);
			$this->load->view('template_admin/navbar');
			$this->load->view('admin/sidebar');
			$this->load->view('admin/editjenis', $data);
			$this->load->view('template_admin/footer');
		} else {
			$id = htmlspecialchars($this->input->post('idjenismenu', true));
			$jenismenu = htmlspecialchars($this->input->post('jenismenu', true));

			$data = array(
				'namajenis' => $jenismenu
			);

			$where = array(
				'idjenismenu' => $id
			);

			$this->VarianModel->update($where,$data,'jenismenu');
			$this->session->set_flashdata('pesan','diupdate');
			redirect('varian');
		}
	}

	public function delete($id)
	{
		$where = array('idjenismenu' => $id);
		$this->VarianModel->delete($where, 'jenismenu');
		$this->session->set_flashdata('pesan','dihapus');
		redirect('varian');
	}
}