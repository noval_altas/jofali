<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends CI_Controller {

	public function __construct(){
        parent::__construct();
        if(!$this->session->userdata('username')){
            redirect('login');
        }
    }

	public function index()
	{
		$data ['invoice'] = $this->InvoiceModel->tampil_data();
		$data ['user'] = $this->AuthModel->datauser();
		$data ['title'] = "Invoice";
			$this->load->view('template_admin/header', $data);
			$this->load->view('template_admin/navbar');
			$this->load->view('admin/sidebar');
			$this->load->view('admin/invoice', $data);
			$this->load->view('template_admin/footer');
	}

	public function detinvoice($idinvoice)
	{
		$data ['user'] = $this->AuthModel->datauser();
		$data['invoice'] = $this->InvoiceModel->ambil_id_invoice($idinvoice);
		$data['pesanan'] = $this->InvoiceModel->ambil_id_pesanan($idinvoice);
		$data ['title'] = "Detail Invoice";
		$this->load->view('template_admin/header', $data);
		$this->load->view('template_admin/navbar');
		if ($data['user']['hakakses'] == 1) {
			$this->load->view('admin/sidebar');
		} if ($data['user']['hakakses'] == 3) {
			$this->load->view('kasir/sidebar');
		}
		$this->load->view('admin/detinvoice', $data);
		$this->load->view('template_admin/footer');
	}
}
