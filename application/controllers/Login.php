<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		$this->form_validation->set_rules('username', 'Username', 'required|trim',['required' => 'Username Wajib Diisi']);
		$this->form_validation->set_rules('password', 'Password', 'required|trim',['required' => 'Password Wajib Diisi']);

    	if ($this->form_validation->run() == FALSE) {
    		$this->load->view('login');
    	} else {
    		$username = $this->input->post('username');
			$password = $this->input->post('password');

			$user = $username;
			$pass = $password;

			$cek = $this->LoginModel->cek_login($user, $pass);

			if ($cek->num_rows() > 0){
				foreach ($cek->result() as $ck) {
					$sess_data['username'] = $ck->username;
					$sess_data['nama'] = $ck->nama;
					$sess_data['hakakses'] = $ck->hakakses;
					$sess_data['status'] = $ck->status;

					$this->session->set_userdata($sess_data);
				}
				if ($sess_data['hakakses'] == '1' && $sess_data['status'] == '1') {
					redirect('admin/admin');
				}elseif ($sess_data['hakakses'] == '2' && $sess_data['status'] == '1') {
					redirect('home');
				}elseif ($sess_data['hakakses'] == '3' && $sess_data['status'] == '1') {
					redirect('kasir/kasir');
				}elseif ($sess_data['hakakses'] == '4' && $sess_data['status'] == '1') {
					redirect('manager');
				}else{
					if ($sess_data['status'] == '0') {
						$this->session->set_flashdata('pesan','
						<div class="alert alert-danger alert-dismissible fade show" role="alert">
		  					Akun anda telah Non Aktif...!!!<br>Segera Lapor Ke-Admin Untuk Mengaktikan Kembali
						  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    	<span aria-hidden="true">&times;</span>
						  	</button>
						</div>');
					}else{
					$this->session->set_flashdata('pesan','
						<div class="alert alert-danger alert-dismissible fade show" role="alert">
		  					Username atau Password Salah...!!!
						  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    	<span aria-hidden="true">&times;</span>
						  	</button>
						</div>');
					}
					redirect('login');
				}
			}else{
				$this->session->set_flashdata('pesan','
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
	  					Username atau Password Salah...!!!
					  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    	<span aria-hidden="true">&times;</span>
					  	</button>
					</div>');
				redirect('login');
			}
    	}
    }

    public function regis()
    {
    	$this->form_validation->set_rules('nama', 'Nama Lengkap', 'required|trim',['required' => 'Nama Lengkap Wajib Diisi']);
    	$this->form_validation->set_rules('username', 'Username', 'required|trim|min_length[3]|is_unique[user.username]',[
    		'required' => 'Username Wajib Diisi',
    		'min_length' => 'Username Terlalu Pendek',
    		'is_unique' => 'Username Telah Terdaftar']);
    	$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[3]|matches[password1]',[
    		'required' => 'Password Wajib Diisi',
    		'matches' => 'Password Tidak Sama',
    		'min_length' => 'Password Terlalu Pendek']);
    	$this->form_validation->set_rules('password1', 'Password', 'required|trim|matches[password]',['required' => 'Password Wajib Diisi']);

    	if ($this->form_validation->run() == FALSE) {
    		$this->load->view('regis');
    	}else{
    		$data = [
    			'nama' => $this->input->post('nama'),
    			'username' => $this->input->post('username'),
    			'password' => $this->input->post('password'),
    			'fotouser' => 'asd.png',
    			'hakakses' => '2',
    			'status' => '1',
    			'created' => date('Y-m-d H:i:s')
    		];

    		$this->db->insert('user', $data);
    		$this->session->set_flashdata('pesan','
						<div class="alert alert-success alert-dismissible fade show" role="alert">
		  					Pendaftaran Sukses,Silahkan Login...!!
						  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    	<span aria-hidden="true">&times;</span>
						  	</button>
						</div>');
    		redirect('login');
    	}
    }

	public function logout()
	{
		// $user = $this->db->get_where('user', ['username' => $username, 'password' => $password])->row_array();
		// $this->session->unset_userdata('username');
		$this->session->sess_destroy();
		$this->session->set_flashdata('pesan','<div class="alert bg-success alert-icon-left alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Terima Kasih !</strong> Anda Telah Keluar</div>');
		redirect('login');
	}
}