-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 13 Jan 2021 pada 05.30
-- Versi server: 10.1.38-MariaDB
-- Versi PHP: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jofalicoffe`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenismenu`
--

CREATE TABLE `jenismenu` (
  `idjenismenu` int(11) NOT NULL,
  `namajenis` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenismenu`
--

INSERT INTO `jenismenu` (`idjenismenu`, `namajenis`) VALUES
(1, 'ICE'),
(2, 'HOT');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu`
--

CREATE TABLE `menu` (
  `idmenu` int(12) NOT NULL,
  `namamenu` varchar(100) NOT NULL,
  `hargamenu` int(15) NOT NULL,
  `fotomenu` varchar(100) NOT NULL,
  `idjenismenu` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `menu`
--

INSERT INTO `menu` (`idmenu`, `namamenu`, `hargamenu`, `fotomenu`, `idjenismenu`) VALUES
(1, 'Ice Taro', 13750, 'Ice_Tato.jpg', 1),
(2, 'Hot Taro', 13750, 'Hot_Taro.jpg', 2),
(3, 'Hot Red Velvet', 13750, 'Hot_Red_Velvet.jpg', 2),
(4, 'Ice Red Velvet', 13750, 'Ice_Red_Velvet.jpg', 1),
(5, 'Ice Cotton Candy', 13750, 'Ice_Cotton_Candy.jpg', 1),
(6, 'Hot Cotton Candy', 13750, 'Hot_Cotton_Candy.jpg', 2),
(7, 'Hot Charcoal', 13750, 'Hot_Charcoal.jpg', 2),
(8, 'Ice Charcoal', 13750, 'Ice_Charcoal.jpg', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_invoice`
--

CREATE TABLE `tb_invoice` (
  `idinvoice` int(12) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(254) NOT NULL,
  `notlp` varchar(20) NOT NULL,
  `pesan` varchar(254) NOT NULL,
  `aksi` varchar(30) NOT NULL,
  `hargatotal` int(11) NOT NULL,
  `tglpesan` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_invoice`
--

INSERT INTO `tb_invoice` (`idinvoice`, `nama`, `alamat`, `notlp`, `pesan`, `aksi`, `hargatotal`, `tglpesan`) VALUES
(1, 'Noval Altas', '', '082337334990', '', 'ambil', 27500, '2021-01-13 07:46:59'),
(2, 'Noval Altas', 'Jl. Merbabu A-5', '082337334990', '', 'dikirim', 18750, '2021-01-13 07:52:53'),
(3, 'Erisa Putri Maulina', '', '082374376987234', '', 'ambil', 27500, '2021-01-13 11:12:23'),
(4, 'Erisa Putri Maulina', 'Surabaya', '08321132322', '', 'dikirim', 46250, '2021-01-13 11:13:27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pesanan`
--

CREATE TABLE `tb_pesanan` (
  `id` int(11) NOT NULL,
  `id_invoice` int(11) NOT NULL,
  `id_brg` int(11) NOT NULL,
  `nama_brg` varchar(50) NOT NULL,
  `jumlah` int(3) NOT NULL,
  `harga` int(10) NOT NULL,
  `pilihan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pesanan`
--

INSERT INTO `tb_pesanan` (`id`, `id_invoice`, `id_brg`, `nama_brg`, `jumlah`, `harga`, `pilihan`) VALUES
(1, 1, 3, 'Hot Red Velvet', 1, 13750, ''),
(2, 1, 1, 'Ice Taro', 1, 13750, ''),
(3, 2, 5, 'Ice Cotton Candy', 1, 13750, ''),
(4, 3, 2, 'Hot Taro', 1, 13750, ''),
(5, 3, 3, 'Hot Red Velvet', 1, 13750, ''),
(6, 4, 4, 'Ice Red Velvet', 2, 13750, ''),
(7, 4, 5, 'Ice Cotton Candy', 1, 13750, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(12) NOT NULL,
  `nama` varchar(70) NOT NULL,
  `username` varchar(90) NOT NULL,
  `password` varchar(90) NOT NULL,
  `alamat` varchar(254) NOT NULL,
  `fotouser` varchar(50) NOT NULL,
  `hakakses` enum('1','2','3','4') NOT NULL DEFAULT '2',
  `status` enum('1','0') NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `nama`, `username`, `password`, `alamat`, `fotouser`, `hakakses`, `status`, `created`) VALUES
(1, 'admin', 'admin', 'admin', '', 'asd.png', '1', '1', '2021-01-12 11:25:44'),
(2, 'kasir', 'kasir', 'kasir', '', 'asd.png', '3', '1', '2021-01-12 11:26:49'),
(3, 'Noval Altas', 'novalaltas', 'novalaltas', 'Jl. Merbabu A-5', 'asd.png', '2', '1', '2021-01-13 01:17:22'),
(5, 'Erisa Putri Maulina', 'erisa2000', 'erisa2000', 'Surabaya', 'asd.png', '2', '1', '2021-01-13 11:11:01');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `jenismenu`
--
ALTER TABLE `jenismenu`
  ADD PRIMARY KEY (`idjenismenu`);

--
-- Indeks untuk tabel `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`idmenu`);

--
-- Indeks untuk tabel `tb_invoice`
--
ALTER TABLE `tb_invoice`
  ADD PRIMARY KEY (`idinvoice`);

--
-- Indeks untuk tabel `tb_pesanan`
--
ALTER TABLE `tb_pesanan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `jenismenu`
--
ALTER TABLE `jenismenu`
  MODIFY `idjenismenu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `menu`
--
ALTER TABLE `menu`
  MODIFY `idmenu` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `tb_invoice`
--
ALTER TABLE `tb_invoice`
  MODIFY `idinvoice` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tb_pesanan`
--
ALTER TABLE `tb_pesanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
